import Repository from './Repository';

const resource = '/user';

export default {
    exists(userName: string, successCb: any, errorCb: any): void {
        Repository.get(`${resource}/exists?userName=${encodeURIComponent(userName)}`)
            .then((response) => {
                return successCb(response.status === 200);
            })
            .catch((error) => {
                return errorCb(error);
            });
    },
};
