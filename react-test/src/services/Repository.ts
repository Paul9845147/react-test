import axios from 'axios';

const baseDomain: string = 'http://api.theappbuilder.local:2224';
const baseURL: string =  `${baseDomain}/api`;

export default axios.create({
    baseURL,
    timeout: 30000,
});
