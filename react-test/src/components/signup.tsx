import React, { Component } from 'react'
import UserService from '../services/UserService';

type MyProps = {};
type MyState = { 
    email: string, 
    isValid:boolean,
    errorMsg:string,
    finished:boolean
};

class Signup extends Component<MyProps, MyState>  {

    constructor(props:any) {
        super(props);
        this.state = {
            email: '', 
            isValid: false,
            errorMsg: '',
            finished: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.errorBox = this.errorBox.bind(this);
    }

    handleChange(event:any) {
        this.setState({email: event.target.value});
    }

    handleSubmit(event:any) {

        console.log('A name was submitted: ' + this.state.email);
        event.preventDefault();

        if (this.validateEmail()){
            this.callUserService();
        }
        else{
            this.setState({errorMsg: 'Invalid email - please try again', email: ''})
        }
        
    }

    callUserService(){

        UserService.exists(
            this.state.email,
            (success: boolean) => {
               console.log(success)
               
               this.setState({finished: true})

            },
            (error: any) => {
                this.setState({errorMsg: 'Email not found - please try again', email: ''})
                console.log(error)
            });

    }

    validateEmail() {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(this.state.email).toLowerCase());
    }

    errorBox(){
        if (this.state.errorMsg != ""){
            return(
                <div className="alert alert-danger" role="alert">
                    {this.state.errorMsg}
                </div>
            )
        }
    }

    componentDidMount() {}

    render() {
        return (
            <div>
                <h2>Signup</h2>
                <p>Please fill out the form below to sign-up</p>

                {this.errorBox()} 
             
                {!this.state.finished &&
                    <form onSubmit={this.handleSubmit}>
                        <label>
                            Email:&nbsp;&nbsp;
                            <input type="text" value={this.state.email} onChange={this.handleChange} placeholder="Enter your email" />
                        </label>&nbsp;
                        <input type="submit" value="Submit" />
                    </form>
                }
                
                
                {this.state.finished &&
                  <div className="alert alert-success" role="alert">
                    Email found - you are signed up!
                  </div>
                }

            </div>
        )
    }
}

export default Signup;