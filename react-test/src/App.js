import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import './App.css';

import AboutUs from './components/aboutus';
import Signup from './components/signup';

function Index() {
  return <h2>Home</h2>;
}

function Users() {
  return <h2>Users</h2>;
}

function AppRouter() {
  return (
    <Router>
      <div>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">

              <div className="collapse navbar-collapse" id="navbarColor01">
                <ul className="navbar-nav mr-auto">

                    <li className="nav-item">
                      <Link className="nav-link"  to="/">Home</Link>
                    </li>
                    <li className="nav-item">
                      <Link className="nav-link" to="/signup/">Signup</Link>
                    </li>
                    <li className="nav-item">
                      <Link className="nav-link" to="/about/">About</Link>
                    </li>
                    <li className="nav-item">
                      <Link className="nav-link" to="/users/">Users</Link>
                    </li>
                  
                </ul>            
              </div>
            </nav>

            <div className="container">
              <Route path="/" exact component={Index} />
              <Route path="/signup/" component={Signup} />
              <Route path="/about/" component={AboutUs} />
              <Route path="/users/" component={Users} />
            </div>

      </div>
    </Router>
  );
}

export default AppRouter;
